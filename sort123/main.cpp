// copy content in a file named main.cpp
//compile with:
// g++ -std=c++11 -Wall main.cpp -o sort123
// run with ./sort123

#include <iostream>
#include <vector>
#include <random>

#define LOGTARGET std::cout

#include <chrono> // Zeitnahme
#define CR std::chrono
#define MS CR::microseconds
#define CLOCK_HR CR::high_resolution_clock
#define TIME_DURATION(x) CR::duration_cast<MS>(x).count()
#define TimePoint CLOCK_HR::time_point
#define Duration CLOCK_HR::duration
#define ZEITNAHME(x) TimePoint x = CLOCK_HR::now();

#define START_TIMER ZEITNAHME(START__TIME)
#define STOP_TIMER ZEITNAHME(STOP__TIME)	   \
  Duration DIFF_TIME = STOP__TIME - START__TIME;   \
  LOGTARGET << "Dauer: "			   \
  << TIME_DURATION(DIFF_TIME)/1000000.0		   \
  << " Sekunden.\n";

using v_vint = std::vector<std::vector<int>>;

class Zufall
{
	using engine = std::default_random_engine;
	using distribution = std::uniform_int_distribution<int>;
	using v_distribution = std::vector<distribution>;
	
	
	// Zufallsgenerator definieren
	engine generator;
	distribution verteiler; 
	v_distribution v_verteiler;
public:
	
	Zufall(int von, int bis);
	Zufall(v_vint verteiler_col);
	// obj() / obj(index) gibt die Zufallszahl zurück
	int operator()(unsigned int index=0); 
};

std::vector<int>& sort123(std::vector<int>& l);

int main(int argc, char* argv[]){
  Zufall z(1,3);
  std::vector<int> sortiere;
  for (int i=0; i < 1000000000; ++i){
    sortiere.push_back( z() );
  }
  
  START_TIMER
    
  sort123(sortiere);

  STOP_TIMER
  
  return 0;
}

Zufall::Zufall(int von,int bis)
 : Zufall( v_vint{ {von,bis} } )
{}

Zufall::Zufall(v_vint verteiler_col){
	// Startwert zeitabhängig setzen
	generator.seed( time(nullptr)*time(nullptr) );
	
	// Vector von Verteilern
	for(auto v : verteiler_col){
		v_verteiler.push_back(distribution( v.at(0), v.at(1) ) );
	}
}

int Zufall::operator()(unsigned int index){
	return v_verteiler.at(index)(generator);
}

std::vector<int>& sort123(std::vector<int>& l){
  const unsigned int min = 1;
  const unsigned int max = 3;
  unsigned int position_next_min  = 0;
  unsigned int position_first_max = l.size();
  std::vector<int>::iterator i = l.begin();
  while( i < l.begin()+position_first_max){
    switch(*i){
    case min:
      if (i != l.begin() + position_next_min) {
        *i = l.at(position_next_min);
        l.at(position_next_min) = min;
      }
      ++i;
      ++position_next_min;
      break;
    case max:
      *i  = l.at(--position_first_max);
      l.at(position_first_max) = max;
      break;
    default:
     ++i;
    }
  }
  return l;
}

// output:
// Dauer: 27.0441 Sekunden.
